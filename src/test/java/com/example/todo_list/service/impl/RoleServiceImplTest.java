package com.example.todo_list.service.impl;

import com.example.todo_list.repository.RoleRepository;
import com.example.todo_list.repository.UserRepository;
import com.example.todo_list.service.RoleService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class RoleServiceImplTest {

    private final RoleService roleService;

    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    RoleServiceImplTest(RoleService roleService) {
        this.roleService = roleService;
    }

    @Test
    void findAll() {
        roleService.findAll();
        Mockito.verify(roleRepository,Mockito.times(1)).findAll();
    }
}
package com.example.todo_list.service.impl;

import com.example.todo_list.repository.RoleRepository;
import com.example.todo_list.repository.UserRepository;
import com.example.todo_list.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class UserServiceImplTest {

    private final UserService userService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private RoleRepository roleRepository;

    @Autowired
    UserServiceImplTest(UserService userService) {
        this.userService = userService;
    }

    @Test
    void findUserByLogin() {
        userService.findUserByLogin("user");
        Mockito.verify(userRepository,Mockito.times(1)).findUserByLogin(ArgumentMatchers.any(String.class));
    }
}
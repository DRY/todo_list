package com.example.todo_list.service.impl;

import com.example.todo_list.entity.Task;
import com.example.todo_list.entity.TaskList;
import com.example.todo_list.repository.RoleRepository;
import com.example.todo_list.repository.TaskRepository;
import com.example.todo_list.repository.UserRepository;
import com.example.todo_list.service.TaskService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class TaskServiceImplTest {

    private final TaskService taskService;

    @MockBean
    private TaskRepository taskRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private RoleRepository roleRepository;

    @Autowired
    TaskServiceImplTest(TaskService taskService) {
        this.taskService = taskService;
    }

    @Test
    void getAllByTaskList() {
        taskService.getAllByTaskList(new TaskList());
        Mockito.verify(taskRepository,Mockito.times(1)).getAllByTaskList(Mockito.any(TaskList.class));
    }

    @Test
    void save() {
        taskService.save(new Task());
        Mockito.verify(taskRepository,Mockito.times(1)).save(Mockito.any(Task.class));
    }

    @Test
    void delete() {
        taskService.delete(new Task());
        Mockito.verify(taskRepository,Mockito.times(1)).delete(Mockito.any(Task.class));
    }

    @Test
    void getById() {
        taskService.getById(1L);
        Mockito.verify(taskRepository,Mockito.times(1)).getById(Mockito.anyLong());
    }
}
package com.example.todo_list.service.impl;

import com.example.todo_list.entity.TaskList;
import com.example.todo_list.entity.User;
import com.example.todo_list.repository.RoleRepository;
import com.example.todo_list.repository.TaskListRepository;
import com.example.todo_list.repository.UserRepository;
import com.example.todo_list.service.TaskListService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class TaskListServiceImplTest {

    private final TaskListService taskListService;

    @MockBean
    private TaskListRepository taskListRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private RoleRepository roleRepository;


    @Autowired
    TaskListServiceImplTest(TaskListService taskListService) {
        this.taskListService = taskListService;
    }

    @Test
    void getAll() {
        taskListService.getAll();
        Mockito.verify(taskListRepository,Mockito.times(1)).findAll();
    }

    @Test
    void getTaskListByUser() {
        taskListService.getTaskListByUser(new User());
        Mockito.verify(taskListRepository,Mockito.times(1)).findAllByUser(Mockito.any(User.class));
    }

    @Test
    void getTaskListById() {
        taskListService.getTaskListById(1L);
        Mockito.verify(taskListRepository,Mockito.times(1)).findById(Mockito.anyLong());
    }

    @Test
    void save() {
        taskListService.save(new TaskList());
        Mockito.verify(taskListRepository,Mockito.times(1)).save(Mockito.any(TaskList.class));
    }

    @Test
    void delete() {
        taskListService.delete(new TaskList());
        Mockito.verify(taskListRepository,Mockito.times(1)).delete(Mockito.any(TaskList.class));
    }

    @Test
    void getTaskListByHeadingExists() {
        taskListService.getTaskListByHeadingExists("test");
        Mockito.verify(taskListRepository,Mockito.times(1)).findAllByHeadingExists(Mockito.anyString());
    }

    @Test
    void getTaskListByHeadingExistsAndUser() {
        taskListService.getTaskListByHeadingExistsAndUser("test", new User());
        Mockito.verify(taskListRepository,Mockito.times(1)).findAllByHeadingExistsAndUser(Mockito.anyString(), Mockito.any(User.class));
    }
}
package com.example.todo_list.controller;

import com.example.todo_list.entity.Task;
import com.example.todo_list.entity.TaskList;
import com.example.todo_list.entity.User;
import com.example.todo_list.pojo.RoleEnum;
import com.example.todo_list.service.TaskListService;
import com.example.todo_list.service.TaskService;
import com.example.todo_list.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/lists")
public class ListController {

    private final TaskListService taskListService;
    private final UserService userService;
    private final TaskService taskService;

    @Autowired
    ListController(TaskListService taskListService, UserService userService, TaskService taskService) {
        this.taskListService = taskListService;
        this.userService = userService;
        this.taskService = taskService;
    }

    @GetMapping("")
    public String lists(Map<String, Object> model, @RequestParam(required = false) String filter) {
        User currentUser = userService.getCurrentUser();
        int privilege = (currentUser!=null && RoleEnum.ADMIN.name().equalsIgnoreCase(currentUser.getRole().getName()))? 1 : 0;
        Iterable<TaskList> lists;
        if (privilege == 1) {
            if (filter == null) {
                lists = taskListService.getAll();
            }
            else {
                lists = taskListService.getTaskListByHeadingExists(filter);
            }
        }
        else {
            if (filter == null) {
                lists = taskListService.getTaskListByUser(currentUser);
            }
            else {
                lists = taskListService.getTaskListByHeadingExistsAndUser(filter, currentUser);
            }
        }
        model.put("privilege", privilege);
        model.put("lists", lists);
        model.put("filter", filter);
        return "home";
    }

    @GetMapping("/add")
    public String add(Map<String, Object> model) {
        User currentUser = userService.getCurrentUser();
        int privilege = (currentUser!=null && RoleEnum.ADMIN.name().equalsIgnoreCase(currentUser.getRole().getName()))? 1 : 0;
        if (privilege == 1)
            return "redirect:/error/403";
        TaskList taskList = new TaskList();
        model.put("taskList", taskList);
        return "add_task_list";
    }

    @PostMapping("/add")
    public String add(@ModelAttribute("taskList") TaskList taskList) {
        User currentUser = userService.getCurrentUser();
        int privilege = (currentUser!=null && RoleEnum.ADMIN.name().equalsIgnoreCase(currentUser.getRole().getName()))? 1 : 0;
        if (privilege == 1)
            return "redirect:/error/403";
        taskList.setUser(userService.getCurrentUser());
        taskListService.save(taskList);
        return "redirect:/lists/" + taskList.getId();
    }

    @GetMapping("/{listId}")
    public String listTasks(@PathVariable Long listId, Map<String, Object> model) {
        TaskList taskList = taskListService.getTaskListById(listId);
        if (!checkPrivilege(taskList)) {
            return "redirect:/error/403";
        }
        Iterable<Task> tasks = taskService.getAllByTaskList(taskList);
        Task newTask = new Task();
        newTask.setTaskList(taskList);
        newTask.setStatus(false);
        model.put("taskList", taskList);
        model.put("tasks", tasks);
        model.put("newTask", newTask);
        return "task_list";
    }

    @GetMapping("/{listId}/edit")
    public String edit(@PathVariable Long listId, Map<String, Object> model) {
        TaskList taskList = taskListService.getTaskListById(listId);
        if (!checkPrivilege(taskList)) {
            return "redirect:/error/403";
        }
        Iterable<Task> tasks = taskService.getAllByTaskList(taskList);
        model.put("taskList", taskList);
        model.put("tasks", tasks);
        return "edit_task_list";
    }

    @PostMapping("/edit")
    public String edit(@ModelAttribute("taskList") TaskList taskList) {
        taskListService.save(taskList);
        return "redirect:/lists/" + taskList.getId();
    }

    @GetMapping("/{listId}/delete")
    public String edit(@PathVariable Long listId) {
        TaskList taskList = taskListService.getTaskListById(listId);
        if (!checkPrivilege(taskList)) {
            return "redirect:/error/403";
        }
        taskListService.delete(taskList);
        return "redirect:/lists";
    }

    private boolean checkPrivilege(TaskList taskList) {
        User currentUser = userService.getCurrentUser();
        int privilege = (currentUser!=null && RoleEnum.ADMIN.name().equalsIgnoreCase(currentUser.getRole().getName()))? 1 : 0;
        if (currentUser.getId() != taskList.getUser().getId() & privilege == 0) {
            return false;
        }
        return true;
    }
}

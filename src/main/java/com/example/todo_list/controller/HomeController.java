package com.example.todo_list.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/home")
    public String view() {
        return "redirect:/lists";
    }

    @GetMapping("/")
    public String viewSlash() {
        return "redirect:/lists";
    }

    @GetMapping("")
    public String viewEmpty() {
        return "redirect:/lists";
    }
}

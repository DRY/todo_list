package com.example.todo_list.controller;

import com.example.todo_list.entity.Task;
import com.example.todo_list.entity.TaskList;
import com.example.todo_list.entity.User;
import com.example.todo_list.pojo.RoleEnum;
import com.example.todo_list.service.TaskListService;
import com.example.todo_list.service.TaskService;
import com.example.todo_list.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    private final TaskListService taskListService;
    private final UserService userService;
    private final TaskService taskService;

    @Autowired
    TaskController(TaskListService taskListService, UserService userService, TaskService taskService) {
        this.taskListService = taskListService;
        this.userService = userService;
        this.taskService = taskService;
    }

    @PostMapping("/add")
    public String add(@ModelAttribute("task") Task task) {
        taskService.save(task);

        return "redirect:/lists/" + task.getTaskList().getId();
    }

    @GetMapping("/{taskId}/delete")
    public String add(@PathVariable Long taskId) {
        Task task = taskService.getById(taskId);
        if (!checkPrivilege(task)) {
            return "redirect:/error/403";
        }
        taskService.delete(task);

        return "redirect:/lists/" + task.getTaskList().getId() + "/edit";
    }

    @GetMapping("/{taskId}/updateStatus")
    public String updateStatus(@PathVariable Long taskId) {
        Task task = taskService.getById(taskId);
        if (!checkPrivilege(task)) {
            return "redirect:/error/403";
        }
        task.setStatus(true);
        taskService.save(task);
        return "redirect:/lists/" + task.getTaskList().getId();
    }

    @GetMapping("/{taskId}/edit")
    public String edit(@PathVariable Long taskId, Map<String, Object> model) {
        Task task = taskService.getById(taskId);
        if (!checkPrivilege(task)) {
            return "redirect:/error/403";
        }
        model.put("task", task);
        return "edit_task";
    }

    @PostMapping("/edit")
    public String edit(@ModelAttribute("task") Task task) {
        task.setStatus(false);
        taskService.save(task);
        return "redirect:/lists/" + task.getTaskList().getId() + "/edit";
    }

    private boolean checkPrivilege(Task task) {
        TaskList taskList = taskListService.getTaskListById(task.getTaskList().getId());
        User currentUser = userService.getCurrentUser();
        Integer privilege = (currentUser!=null && RoleEnum.ADMIN.name().equalsIgnoreCase(currentUser.getRole().getName()))? 1 : 0;
        if (currentUser.getId() != taskList.getUser().getId() & privilege == 0) {
            return false;
        }
        return true;
    }
}

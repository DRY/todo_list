package com.example.todo_list.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull(message = "Поле не может быть пустым")
    @Column(name = "login")
    private String login;
    @NotNull(message = "Поле не может быть пустым")
    @Size(min = 6, message = "Не менее 6 символов")
    @Column(name = "password")
    private String password;
    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;
}

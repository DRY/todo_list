package com.example.todo_list.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "task_lists")
public class TaskList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String heading;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

}

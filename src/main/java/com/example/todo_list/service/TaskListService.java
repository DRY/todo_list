package com.example.todo_list.service;

import com.example.todo_list.entity.TaskList;
import com.example.todo_list.entity.User;

import java.util.List;

public interface TaskListService {
    List<TaskList> getAll();
    List<TaskList> getTaskListByUser(User user);
    TaskList getTaskListById(long id);
    void save(TaskList taskList);
    void delete(TaskList taskList);
    List<TaskList> getTaskListByHeadingExists(String findStr);
    List<TaskList> getTaskListByHeadingExistsAndUser(String findStr, User user);
}

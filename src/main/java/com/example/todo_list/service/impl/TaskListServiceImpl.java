package com.example.todo_list.service.impl;

import com.example.todo_list.entity.TaskList;
import com.example.todo_list.entity.User;
import com.example.todo_list.repository.TaskListRepository;
import com.example.todo_list.repository.UserRepository;
import com.example.todo_list.service.TaskListService;
import com.example.todo_list.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class TaskListServiceImpl implements TaskListService {

    private final TaskListRepository taskListRepository;

    @Autowired
    TaskListServiceImpl(TaskListRepository taskListRepository) {
        this.taskListRepository = taskListRepository;
    }

    @Override
    public List<TaskList> getAll() {
        return taskListRepository.findAll();
    }

    @Override
    public List<TaskList> getTaskListByUser(User user) {
        return taskListRepository.findAllByUser(user);
    }

    @Override
    public TaskList getTaskListById(long id) {
        return taskListRepository.findById(id);
    }

    @Override
    public void save(TaskList taskList) {
        taskListRepository.save(taskList);
    }

    @Override
    public void delete(TaskList taskList) {
        taskListRepository.delete(taskList);
    }

    @Override
    public List<TaskList> getTaskListByHeadingExists(String findStr) {
        return taskListRepository.findAllByHeadingExists(findStr);
    }

    @Override
    public List<TaskList> getTaskListByHeadingExistsAndUser(String findStr, User user) {
        return taskListRepository.findAllByHeadingExistsAndUser(findStr, user);
    }
}

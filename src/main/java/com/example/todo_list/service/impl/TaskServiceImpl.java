package com.example.todo_list.service.impl;


import com.example.todo_list.entity.Task;
import com.example.todo_list.entity.TaskList;
import com.example.todo_list.repository.TaskRepository;
import com.example.todo_list.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;

    @Autowired
    TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> getAllByTaskList(TaskList taskList) {
        return taskRepository.getAllByTaskList(taskList);
    }

    @Override
    public void save(Task task) {
        taskRepository.save(task);
    }

    @Override
    public void delete(Task task) {
        taskRepository.delete(task);
    }

    @Override
    public Task getById(long id) {
        return taskRepository.getById(id);
    }
}

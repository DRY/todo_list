package com.example.todo_list.service;

import com.example.todo_list.entity.Task;
import com.example.todo_list.entity.TaskList;

import java.util.List;

public interface TaskService {
    List<Task> getAllByTaskList(TaskList taskList);
    void save(Task task);
    void delete(Task task);
    Task getById(long id);
}

package com.example.todo_list.service;

import com.example.todo_list.entity.User;

public interface UserService {
    User findUserByLogin(String username);
    User getCurrentUser();
}

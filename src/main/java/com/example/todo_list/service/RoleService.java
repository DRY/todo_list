package com.example.todo_list.service;

import com.example.todo_list.entity.Role;

import java.util.List;

public interface RoleService {
    List<Role> findAll();
}

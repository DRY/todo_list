package com.example.todo_list.repository;

import com.example.todo_list.entity.Task;
import com.example.todo_list.entity.TaskList;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TaskRepository extends CrudRepository<Task, Long> {
    List<Task> getAllByTaskList(TaskList taskList);
    Task getById(long id);
}

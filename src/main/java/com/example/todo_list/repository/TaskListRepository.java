package com.example.todo_list.repository;

import com.example.todo_list.entity.TaskList;
import com.example.todo_list.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TaskListRepository extends JpaRepository<TaskList, Long> {
    List<TaskList> findAll();
    List<TaskList> findAllByUser(User user);
    TaskList findById(long id);
    @Query("SELECT tl FROM TaskList tl LEFT JOIN Task t ON tl.id = t.taskList.id " +
            "WHERE tl.heading LIKE %:findStr% OR t.description LIKE %:findStr%")
    List<TaskList> findAllByHeadingExists(String findStr);
    @Query("SELECT tl FROM TaskList tl LEFT JOIN Task t ON tl.id = t.taskList.id " +
            "WHERE (tl.heading LIKE %:findStr% OR t.description LIKE %:findStr%) " +
            "AND tl.user = :user")
    List<TaskList> findAllByHeadingExistsAndUser(String findStr, User user);
}

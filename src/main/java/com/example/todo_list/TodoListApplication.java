package com.example.todo_list;

import com.example.todo_list.entity.Role;
import com.example.todo_list.entity.User;
import com.example.todo_list.pojo.RoleEnum;
import com.example.todo_list.repository.RoleRepository;
import com.example.todo_list.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class TodoListApplication {

    public static void main(String[] args) {
        SpringApplication.run(TodoListApplication.class, args);
    }

    @Bean
    CommandLineRunner runner(RoleRepository roleRepository, UserRepository userRepository) {
        return args -> {
            Role admin = new Role();
            admin.setName(RoleEnum.ADMIN.name());
            roleRepository.save(admin);
            Role userRole = new Role();
            userRole.setName(RoleEnum.USER.name());
            roleRepository.save(userRole);
            User user1 = new User();
            user1.setLogin("user1");
            user1.setPassword(new BCryptPasswordEncoder().encode("1"));
            user1.setRole(userRole);
            userRepository.save(user1);
            User user2 = new User();
            user2.setLogin("user2");
            user2.setPassword(new BCryptPasswordEncoder().encode("admin1"));
            user2.setRole(admin);
            userRepository.save(user2);
            User user3 = new User();
            user3.setLogin("user3");
            user3.setPassword(new BCryptPasswordEncoder().encode("1"));
            user3.setRole(userRole);
            userRepository.save(user3);
        };
    }


}
